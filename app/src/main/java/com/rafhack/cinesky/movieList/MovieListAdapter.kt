package com.rafhack.cinesky.movieList

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestOptions
import com.rafhack.cinesky.R
import com.rafhack.cinesky.data.entities.Movie


class MovieListAdapter(private val movies: ArrayList<Movie>, private val callback: ((Movie) -> Unit))
    : RecyclerView.Adapter<MovieListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.movie_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = movies.size

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bind(position)
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        private val imgMovie = itemView.findViewById<ImageView>(R.id.movie_item_img_movie)
        private val tvwMovie = itemView.findViewById<TextView>(R.id.movie_item_tvw_movie_title)

        fun bind(position: Int) {
            val movie = movies[position]

            var requestOptions = RequestOptions()
            requestOptions =
                    requestOptions
                            .transforms(CenterCrop(), RoundedCorners(16))
                            .placeholder(R.drawable.movie_placeholder)
                            .error(R.drawable.movie_placeholder)

            Glide.with(imgMovie.context)
                    .load(movie.coverURL)
                    .apply(requestOptions)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(imgMovie)

            tvwMovie.text = movie.title

            itemView.setOnClickListener { callback.invoke(movie) }
        }
    }

}