package com.rafhack.cinesky.movieList

import com.rafhack.cinesky.base.BaseContract
import com.rafhack.cinesky.data.entities.Movie

interface MovieListContract {


    interface View : BaseContract.View {
        fun setProgress(active: Boolean)
        fun showErrorMessage()
        fun showMovies(movies: ArrayList<Movie>)
    }

    interface Presenter : BaseContract.Presenter<View> {
        fun getMovieList()
    }

}