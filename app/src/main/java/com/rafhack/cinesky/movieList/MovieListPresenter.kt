package com.rafhack.cinesky.movieList

import com.rafhack.cinesky.data.domain.MovieInteractor
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MovieListPresenter : MovieListContract.Presenter {

    private lateinit var view: MovieListContract.View
    var movieInteractor: MovieInteractor = MovieInteractor()

    override fun getMovieList() {
        view.setProgress(true)
        movieInteractor.getMovies()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    view.showMovies(it)
                    view.setProgress(false)
                }, {
                    view.showErrorMessage()
                    view.setProgress(false)
                })
    }

    override fun attach(view: MovieListContract.View) {
        this.view = view
    }

}