package com.rafhack.cinesky.movieList

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.view.View.GONE
import android.view.View.VISIBLE
import android.widget.ProgressBar
import android.widget.TextView
import com.rafhack.cinesky.R
import com.rafhack.cinesky.data.entities.Movie
import com.rafhack.cinesky.movieDetail.MovieDetailActivity
import org.parceler.Parcels

class MovieListActivity : AppCompatActivity(), MovieListContract.View {

    private val presenter = MovieListPresenter()

    private lateinit var pgbProgress: ProgressBar
    private lateinit var rcvMovies: RecyclerView
    private lateinit var tvwHeading: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_list)
        setupActionBar()

        pgbProgress = findViewById(R.id.activity_movie_list_pgb_progress)
        rcvMovies = findViewById(R.id.activity_movie_list_rcv_movies)
        tvwHeading = findViewById(R.id.activity_movie_list_tvw_heading)

        presenter.attach(this)
        presenter.getMovieList()
    }

    private fun setupActionBar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
    }

    override fun setProgress(active: Boolean) {
        pgbProgress.visibility = if (active) VISIBLE else GONE
    }

    override fun showErrorMessage() {
        Snackbar.make(findViewById(R.id.activity_movie_list_ctl_root),
                R.string.error_message, Snackbar.LENGTH_LONG).show()
    }

    override fun showMovies(movies: ArrayList<Movie>) {
        tvwHeading.visibility = VISIBLE
        rcvMovies.adapter = MovieListAdapter(movies) {
            val intent = Intent(this, MovieDetailActivity::class.java)
            intent.putExtra("movie", Parcels.wrap(it))
            startActivity(intent)
        }
    }

}