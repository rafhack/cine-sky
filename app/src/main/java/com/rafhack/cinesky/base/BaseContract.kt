package com.rafhack.cinesky.base

class BaseContract {

    interface View

    interface Presenter<T : BaseContract.View> {
        fun attach(view: T)
    }

}