package com.rafhack.cinesky.data.domain

import com.rafhack.cinesky.data.entities.Movie
import com.rafhack.cinesky.data.remote.ServiceGenerator
import com.rafhack.cinesky.data.remote.services.MovieListService
import io.reactivex.Single

class MovieInteractor {

    private val service get() = ServiceGenerator.createService(MovieListService::class.java)

    fun getMovies(): Single<ArrayList<Movie>> {
        return service.getMovies()
    }

}