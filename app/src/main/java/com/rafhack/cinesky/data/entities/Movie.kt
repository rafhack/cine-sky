package com.rafhack.cinesky.data.entities

import com.google.gson.annotations.SerializedName
import org.parceler.Parcel

@Parcel
data class Movie(
        var id: String = "",
        var title: String = "",
        var overview: String = "",
        var duration: String = "",
        @SerializedName("release_year")
        var releaseYear: String = "",
        @SerializedName("cover_url")
        var coverURL: String = "",
        @SerializedName("backdrops_url")
        var backdropsURL: ArrayList<String> = arrayListOf()
)