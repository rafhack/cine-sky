package com.rafhack.cinesky.data.remote.services

import com.rafhack.cinesky.data.entities.Movie
import io.reactivex.Single
import retrofit2.http.GET

interface MovieListService {
    @GET("Movies")
    fun getMovies(): Single<ArrayList<Movie>>
}