package com.rafhack.cinesky.movieDetail

import android.os.Bundle
import android.os.Handler
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.webkit.WebView
import android.widget.TextView
import com.rafhack.cinesky.R
import com.rafhack.cinesky.data.entities.Movie
import org.parceler.Parcels


class MovieDetailActivity : AppCompatActivity() {

    private lateinit var movie: Movie

    private lateinit var wbvOverview: WebView
    private lateinit var vpgBackdrop: ViewPager
    private lateinit var tvwReleaseYear: TextView
    private lateinit var tvwDuration: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie_detail)
        movie = Parcels.unwrap(intent.getParcelableExtra("movie"))
        setupToolbar()
        setupView()
    }

    private fun setupView() {
        wbvOverview = findViewById(R.id.activity_movie_detail_wbv_overview)
        vpgBackdrop = findViewById(R.id.activity_movie_detail_vpg_backdrops)
        tvwReleaseYear = findViewById(R.id.activity_movie_detail_tvw_release_year)
        tvwDuration = findViewById(R.id.activity_movie_detail_tvw_duration)

        val titleLabel = getString(R.string.movie_detail_overview, movie.overview)
        wbvOverview.loadData(titleLabel, "text/html", "UTF-8")

        vpgBackdrop.adapter = MovieDetailBackdropAdapter(movie.backdropsURL)
        val hwnd = Handler()
        hwnd.postDelayed(object : Runnable {
            override fun run() {
                vpgBackdrop.currentItem = if (vpgBackdrop.currentItem + 1 == vpgBackdrop.adapter?.count)
                    0 else vpgBackdrop.currentItem + 1
                hwnd.postDelayed(this, 5000)
            }
        }, 5000)

        tvwReleaseYear.text = movie.releaseYear
        tvwDuration.text = movie.duration
    }

    private fun setupToolbar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.title = movie.title
        supportActionBar?.setDisplayShowHomeEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return super.onSupportNavigateUp()
    }


}