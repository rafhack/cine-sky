package com.rafhack.cinesky.movieDetail

import android.support.v4.view.PagerAdapter
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions

class MovieDetailBackdropAdapter(private val backdrops: ArrayList<String>) : PagerAdapter() {

    override fun isViewFromObject(view: View, `object`: Any): Boolean = view == `object`

    override fun getCount(): Int = backdrops.size

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val imageBackdrop = ImageView(container.context)
        imageBackdrop.scaleType = ImageView.ScaleType.CENTER_CROP
        Glide.with(imageBackdrop.context)
                .load(backdrops[position])
                .transition(DrawableTransitionOptions.withCrossFade())
                .into(imageBackdrop)
        container.addView(imageBackdrop)
        return imageBackdrop
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View?)
    }
}