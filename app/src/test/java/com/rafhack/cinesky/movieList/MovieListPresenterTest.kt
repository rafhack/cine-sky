package com.rafhack.cinesky.movieList

import com.rafhack.cinesky.data.domain.MovieInteractor
import com.rafhack.cinesky.data.entities.Movie
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class MovieListPresenterTest {

    @Mock
    private val view: MovieListContract.View? = null
    @Mock
    private val interactor: MovieInteractor? = null
    private lateinit var presenter: MovieListPresenter

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        presenter = MovieListPresenter()
        presenter.attach(view!!)
        presenter.movieInteractor = interactor!!
    }

    @Test
    fun getMovieListSuccess() {
        val response = Mockito.mock(ArrayList::class.java)
        Mockito.doReturn(Single.just(response)).`when`(interactor)?.getMovies()
        presenter.getMovieList()
        @Suppress("UNCHECKED_CAST")
        Mockito.verify(view!!).showMovies(response as ArrayList<Movie>)
    }

    @Test
    fun getMovieListError() {
        val response = Exception("")
        val single: Single<Throwable> = Single.error(response)
        Mockito.doReturn(single).`when`(interactor)?.getMovies()
        presenter.getMovieList()
        Mockito.verify(view!!).showErrorMessage()
    }

}